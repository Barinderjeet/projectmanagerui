import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskComponent } from './add-task.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ActivatedRoute} from "@angular/router";
import {Observable, of ,throwError} from 'rxjs';
import {RouterTestingModule} from "@angular/router/testing";
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";
import { ServiceService  } from 'src/app/services/service.service';
describe('AddTaskComponent', () => {
  let component: AddTaskComponent;
  let fixture: ComponentFixture<AddTaskComponent>;
let params:Observable<any>;
let route:ActivatedRoute;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ AddTaskComponent ],
      providers:[ServiceService],
      schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskComponent);
    component = fixture.componentInstance;
    route=TestBed.get(ActivatedRoute);
    route.snapshot.params={'TaskID':'12'};
    component.routeParams={'TaskID':'12'};

    let response=[{"ProjectID":19,"Project":"Testing","Priority":3,"ManagerID":1003,"StartDate":"2018-11-02T00:00:00","EndDate":"2018-11-13T00:00:00","Flag":0},{"ProjectID":20,"Project":"Project","Priority":1,"ManagerID":1013,"StartDate":"2018-11-01T00:00:00","EndDate":"2018-11-15T00:00:00","Flag":0},{"ProjectID":21,"Project":"Project1","Priority":1,"ManagerID":1008,"StartDate":"2018-11-05T00:00:00","EndDate":"2018-11-21T00:00:00","Flag":0}];
    spyOn(component['ServiceService'],'getByTaskId').and.returnValues(of(response),throwError(new Error('error')));
    let projectDataResponse=[{"ProjectID":19,"Project":"Testing","Priority":4,"ManagerID":1003,"StartDate":"2018-11-02T00:00:00","EndDate":"2018-11-13T00:00:00","Flag":0},{"ProjectID":20,"Project":"Project","Priority":6,"ManagerID":1013,"StartDate":"2018-11-01T00:00:00","EndDate":"2018-11-15T00:00:00","Flag":0},{"ProjectID":21,"Project":"Project1","Priority":1,"ManagerID":1008,"StartDate":"2018-11-05T00:00:00","EndDate":"2018-11-21T00:00:00","Flag":0}];
    spyOn(component['ServiceService'],'getProjectData').and.returnValues(of(projectDataResponse),throwError(new Error('error') ));
   let addResponse="Record Added";
   spyOn(component['ServiceService'],'postParentTaskData').and.returnValues(of(addResponse),throwError(new Error('error') ));
   
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should add the data' ,()=>{
    component.IsParentTask=true;
    component.Add();
    expect(component.IsParentTask).toBeFalsy();


  });
  it('should get the data based on task id',()=>{
    component.TaskDetails={EndDate: "2018-11-15T00:00:00",
    Flag: 0,
    ParentID: 3,
    Priority: 3,
    ProjectID: 19,
    StartDate: "2018-11-15T00:00:00",
    Status: null,
    Task: "Task12",
    TaskID: 1017,
    TaskOwner: 1008};
   //component.TaskDetails={"StartDate":"2018-11-15T00:00:00","EndDate":"2018-11-15T00:00:00","TaskID":"123"};
    // expect(component['ServiceService'].getByTaskId).toHaveBeenCalled();
    component.ngOnInit();
    expect(component.isAdd).toBeTruthy();
    expect(component.projectdata.length).toBeDefined();
  });
  

});
