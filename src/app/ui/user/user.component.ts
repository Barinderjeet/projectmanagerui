import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/User';
import { NgModule } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  sortedOrder: string;
  SortedBy: string;

  constructor(private ServiceService: ServiceService) {

  }
  UserDetails: User[] = [];
  UserInfo: any = {};
  columns: any[] = [];
  edited: boolean = false;
  adddisplay: any = 'block';
  updatedisplay: any = 'none';
  isAdd: boolean= false;
  isUpdate: boolean= true;

  isFirstEmpty: boolean=true;
  isLastEmpty: boolean=true;
  isEmpEmpty: boolean=true;
  
  Search:any;
  ngOnInit() {
    this.columns = this.ServiceService.getColumnsUsers();
    console.log(this.columns);
    this.getUserData();
  }
  getUserData() {
    this.ServiceService.getUserData().subscribe
      (response => {

        console.log("response" + JSON.stringify(response));

        this.UserDetails = response;

        // console.log('kkk'+ this.TaskDetails.StartDate);
      }
      );
  }

  add() {
    
    if((this.UserInfo.EmployeeID=="" || this.UserInfo.EmployeeID==null || this.UserInfo.EmployeeID==undefined)
    && (this.UserInfo.LastName=="" || this.UserInfo.LastName==null || this.UserInfo.LastName==undefined)
    && (this.UserInfo.FirstName=="" || this.UserInfo.FirstName==null || this.UserInfo.FirstName==undefined)){
console.log('firstname'+ this.UserInfo.FirstName);
this.isEmpEmpty=false;
this.isFirstEmpty=false;
this.isLastEmpty=false;
}
else if(this.UserInfo.FirstName=="" || this.UserInfo.FirstName==null || this.UserInfo.FirstName==undefined)
{
  console.log('firstname'+ this.UserInfo.FirstName);
  this.isFirstEmpty=false;
  //this.isLastEmpty=false;
  //
 
}

else if(this.UserInfo.LastName=="" || this.UserInfo.LastName==null || this.UserInfo.LastName==undefined){
  console.log('firstname'+ this.UserInfo.FirstName);
  this.isLastEmpty=false;
}
else if(this.UserInfo.EmployeeID=="" || this.UserInfo.EmployeeID==null || this.UserInfo.EmployeeID==undefined){
    console.log('firstname'+ this.UserInfo.FirstName);
    this.isEmpEmpty=false;
}

 else{
  this.isFirstEmpty=true;
  this.isLastEmpty=true;
  this.isEmpEmpty=true;
    console.log('test' + this.UserInfo);
    this.ServiceService.postUserData(this.UserInfo)
      .subscribe(response => {
        console.log(response);
        this.getUserData();
        alert('User Added!');
      }

      );

    }

  }
  update() {
    this.ServiceService.updateUserData(this.UserInfo)
      .subscribe(response => {
        console.log(response);
        this.getUserData();
        alert('User Updated!');
      }


      );
    this.updatedisplay = 'none';
    this.adddisplay = 'block';

  }
  Edit(UserID) {
    this.isAdd=true;
    this.isUpdate=false;
    this.edited = true;
    this.updatedisplay = 'block';
    this.adddisplay = 'none';
    console.log('test' + this.UserInfo);
    this.UserInfo = {...this.UserDetails.find(p => p.UserID == UserID)};

  }

  Delete(UserID) {
    {
      console.log("taskid", UserID);
       this.ServiceService.deleteUserData(UserID)
       .subscribe(response=> {
        this.UserDetails= response;
         this.getUserData();
      
      }); 
    }
    // this.UserInfo =  this.UserDetails.find(p=>p.UserID == UserID);
    // console.log('test'+  this.UserInfo);
    // this.ServiceService.deleteUserData(this.UserInfo)    
    //     .subscribe(response=>
    //     {
    //       console.log(response);
    //       this.getUserData();
    //       alert('User Deleted!') ;     
    //     }

    //   );
  }

  sortUsers(sortBy: string) {
    console.log(sortBy);
    var sortOrder = this.SortedBy != sortBy || this.sortedOrder == "desc" ? "asc" : "desc";

    this.UserDetails = _.orderBy(this.UserDetails, [sortBy], [sortOrder]);
    console.log(this.UserDetails);
    console.log(sortOrder);

    this.SortedBy = sortBy;

    this.sortedOrder = sortOrder;

  }


  reset(){
    this.isFirstEmpty=true;
    this.isLastEmpty=true;
    this.isEmpEmpty=true;

    this.isAdd= false;
      this.isUpdate= true;
    this.UserInfo.EmployeeID="";
    this.UserInfo.FirstName=null;
     this.UserInfo.LastName=null;
    
     
  }

 

}
