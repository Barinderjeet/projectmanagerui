import { Injectable } from '@angular/core';


import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

// import {Task} from 'src/app/task';
import { Observable } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { stringify } from '@angular/core/src/util';
//import { map } from 'rxjs/operators';
//import 'rxjs/add/operator/map';
//import 'rxjs/Rx';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }
  
  getColumns(): string[]{
    return ["TaskID","ParentID", "ProjectID", "Task", "Priority","StartDate", "EndDate","Status"]};

       getColumnsProjects(): string[]{
      return ["ProjectID", "Project", "Priority","ManagerID","StartDate", "EndDate"]};

       getColumnsUsers(): string[]{
        return ["UserID", "FirstName", "LastName","EmployeeID"]}; 

        getColumnsofParentTask():string[]{
          return ["ParentID", "ParentTask"]};
    getTaskData():Observable<any>
    {
     return this.http.get("http://localhost/ProjectManagerService/GetAll");
   /*
      let obs = this.http.get("http://localhost:64915/GetAll");
      obs.subscrwbbibe((response)=>console.log(response));
      return obs;*/
    }
    getProjectData():Observable<any>
    {
     return this.http.get("http://localhost/ProjectManagerService/GetAllProject");
  
    }

    getUserData():Observable<any>
    {
     return this.http.get("http://localhost/ProjectManagerService/GetAllUsers");
  
    }

    getByTaskId(TaskId:any):Observable<any>
    {
     return this.http.get("http://localhost/ProjectManagerService/GetById?TaskId=" + TaskId);
 
    }
    postUserData(UserDetail:any):Observable<any>
    {
     console.log(UserDetail);  
     return this.http.post("http://localhost/ProjectManagerService/PostUser",
     UserDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
    deleteUserData(UserDetail:any):Observable<any>
    {
      console.log('INSIDE SERVICE' + JSON.stringify(UserDetail) );
     
     //return this.http.delete("http://localhost:50055/DeleteUser",UserDetail);

     return this.http.delete("http://localhost/ProjectManagerService/DeleteUser?id=" + JSON.stringify(UserDetail));
     
     
     
    //  console.log(UserDetail);  
    //  return this.http.delete("http://localhost:50055/DeleteUser",
    //  UserDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
    deleteProjectData(UserDetail:any):Observable<any>
    {
      console.log('INSIDE SERVICE' + JSON.stringify(UserDetail) );
     
     //return this.http.delete("http://localhost:50055/DeleteUser",UserDetail);

     return this.http.delete("http://localhost/ProjectManagerService/DeleteProject?id=" + JSON.stringify(UserDetail));
    }
    updateUserData(UserDetail:any):Observable<any>
    {
     console.log(UserDetail);  
     return this.http.put("http://localhost/ProjectManagerService/UpdateUser",
     UserDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }

    postProjectData(ProjectDetail:any):Observable<any>
    {
     console.log(ProjectDetail);  
     return this.http.post("http://localhost/ProjectManagerService/PostProject",
     ProjectDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
    postTaskData(TaskDetail:any):Observable<any>
    {
     console.log(TaskDetail);  
     return this.http.post("http://localhost/ProjectManagerService/PostTask",
     TaskDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }

    postParentTaskData(ParentTaskDetail:any):Observable<any>
    {
     console.log(ParentTaskDetail);  
     return this.http.post("http://localhost/ProjectManagerService/PostParenTask",
     ParentTaskDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
    getParentTaskData():Observable<any>
    {
     return this.http.get("http://localhost/ProjectManagerService/GetAllParenTask");
  
    }
    putTaskData(TaskDetail:any):Observable<any>
    {
     
     return this.http.put("http://localhost/ProjectManagerService/UpdateTask/",
     TaskDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }

    putProjectData(ProjectDetail:any):Observable<any>
    {
     
     return this.http.put("http://localhost/ProjectManagerService/UpdateProject/",
     ProjectDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
    
    deleteTaskData(UserDetail:any):Observable<any>
    {
     console.log('INSIDE SERVICE' + JSON.stringify(UserDetail) );
     console.log(JSON.stringify(UserDetail));  
     return this.http.delete("http://localhost/ProjectManagerService/DeleteTask?id=" + JSON.stringify(UserDetail)
     
     
     ); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
   endTaskData(UserDetail:any):Observable<any>
   {  
     
     return this.http.put("http://localhost/ProjectManagerService/EndTask/" , UserDetail);
     //.map(res=>res);
    
   }
 
}
