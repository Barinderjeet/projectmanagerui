import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServiceService } from 'src/app/services/service.service';
import { UserComponent } from './user.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {Observable, of ,throwError} from 'rxjs';
import {RouterTestingModule} from "@angular/router/testing";
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";
import { searchUserFilterPipe } from 'src/app/ui/user/UserSearchPipe';
describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ UserComponent,searchUserFilterPipe ],
      schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
      providers:[ServiceService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    let response=[];
    spyOn(component['ServiceService'],'getUserData').and.returnValues(of(response),throwError(new Error('error')));
    fixture.detectChanges();
  });

  it('should create..', () => {
    expect(component).toBeTruthy();
  });
  it("should get the user data",()=>{
    component.getUserData();
    expect(component.UserDetails).toBeDefined();
  });
});
