
export class Task {
    
         TaskID : number;
         ParentID :number;
         ProjectID :number;
         Task :string;
         Priority : number;        
         StartDate: string;
         EndDate : string;
         Status:string;
         Flag :number;
         TaskOwner: number;
         CompletedTask:number;
}


