import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ProjectComponent } from './project.component';
import {Observable, of ,throwError} from 'rxjs';
import {RouterTestingModule} from "@angular/router/testing";
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";
import { ServiceService  } from 'src/app/services/service.service';
describe('ProjectComponent', () => {
  let component: ProjectComponent;
  let fixture: ComponentFixture<ProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ ProjectComponent ],
      schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
      providers:[ServiceService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectComponent);
    component = fixture.componentInstance;
    let response=[{"ProjectID":19,"Project":"Testing","Priority":3,"ManagerID":1003,"StartDate":"2018-11-02T00:00:00","EndDate":"2018-11-13T00:00:00","Flag":0},{"ProjectID":20,"Project":"Project","Priority":1,"ManagerID":1013,"StartDate":"2018-11-01T00:00:00","EndDate":"2018-11-15T00:00:00","Flag":0},{"ProjectID":21,"Project":"Project1","Priority":1,"ManagerID":1008,"StartDate":"2018-11-05T00:00:00","EndDate":"2018-11-21T00:00:00","Flag":0}];
    spyOn(component['ServiceService'],'getTaskData').and.returnValues(of(response),throwError(new Error('error')));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should get the task data',()=>{
    component.ngOnInit();
    expect(component.TaskDetails).toBeDefined();
  });
  
});
