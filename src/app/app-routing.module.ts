import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from './ui/project/project.component';
import { AddTaskComponent } from './ui/add-task/add-task.component';
import { ViewTaskComponent } from './ui/view-task/view-task.component';
import { UserComponent } from './ui/user/user.component';
const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const AppRoutes: Routes =[
  {path: 'addtask',  component: AddTaskComponent},
  {path: 'viewtask',  component: ViewTaskComponent},
  {path: 'project',  component: ProjectComponent},
  {path: 'user',  component: UserComponent},
  { path: 'update/:TaskID', component: AddTaskComponent }
  ];