
    import { Pipe, PipeTransform } from '@angular/core';
    import { NgAnalyzedFile } from "@angular/compiler";
    import { Task } from 'src/app/Task';
import { User } from 'src/app/User';

    @Pipe({

        name: "searchUserFilter"
    })

export class searchUserFilterPipe implements PipeTransform{
        transform(View: User[], Search: string ) {
            if(!View || !Search)
            {
                return View;
            }
            else{
                return View.filter(task=>
                    (Search && task.FirstName.toLowerCase().
                    indexOf(Search.toLowerCase())  != -1
                ||Search && task.LastName.toLowerCase().
                indexOf(Search.toLowerCase())  != -1
            || Search && task.EmployeeID.toString().
            indexOf(Search.toString())  != -1)
    
                );
            }

            
        }


}