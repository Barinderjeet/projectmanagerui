import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/Task';
import { NgModule } from '@angular/core';
import { ServiceService  } from 'src/app/services/service.service';
import { ParenTask } from 'src/app/ParentTask';
import {ActivatedRoute} from "@angular/router";
import { Project } from 'src/app/Project';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})



        
export class AddTaskComponent implements OnInit {
  edited: boolean;
  taskID: any;
  ParentTask: any;
  isEmpty:boolean=true;
  ParentTaskdisplay: string;
  ParentTaskdata: any;
  columnsofParentTask: string[];
  UserName: string;
  userdisplay: string;
  usersdata: any;
  columnsofUsers: string[];
  projectdisplay: string;
  ProjectName: any;
  projectdata: any=[];
  columnsofProject: any ;
  IsParentTask:boolean= true;
  parentTask: any;
  isUpdate: boolean= true;
  isAdd: boolean=false;
  isTaskEmpty: boolean=true;
  isProject: boolean=true;
  isParentTask: boolean=true;
  isUser: boolean=true;

  routeParams;
  constructor(private ServiceService: ServiceService, private route : ActivatedRoute) {
    // this.route.params.subscribe( params => { this.taskID = params.TaskID;
    //   console.log("this.taskID",params);
    //  });
    this.routeParams=this.route.snapshot.params;
    this.taskID=  this.routeParams.TaskID ;

   }
  ParentTaskDetail: ParenTask= {ParentID:0, ParentTask:null};
 TaskDetails : any = { TaskID:0, ParentID:null, ProjectID:null, Task:null, Priority:null, StartDate:null, EndDate:null, Flag:0, Status:null,TaskOwner:null,CompletedTask:null};
  //TaskDetails : any ;
  
  ngOnInit() {
    
    
    this.projectdisplay='none';
    this.userdisplay='none';
    this.ParentTaskdisplay='none';
  this.taskID=  this.route.snapshot.params.TaskID ;
  console.log('edit'+ this.taskID);
    if(this.route.snapshot.params.TaskID != undefined)
    {
     // this.isUpdate=true;
      this.edited = true;
      this.IsParentTask=false;
      this.ServiceService.getByTaskId(this.taskID).subscribe(response=>
        {
          this.isAdd=true;
          this.isUpdate=false;
          console.log("task id response",response);
          this.TaskDetails = response;
          //this.TaskDetails.StartDate = this.TaskDetails.StartDate.toISOString().substr(0,10);
          //sthis.TaskDetails.EndDate = this.TaskDetails.EndDate.substr(0,10);
          console.log('project ID=' + this.TaskDetails);
          console.log('type this.TaskDetails.StartDate', typeof this.TaskDetails.StartDate) 
           this.TaskDetails.StartDate = this.TaskDetails.StartDate.split('T')[0];
           this.TaskDetails.EndDate = this.TaskDetails.EndDate.split('T')[0];
          this.ServiceService.getProjectData().subscribe
          (response=>
            {

            console.log("ProjectData**** " + JSON.stringify(response) + "   " + this.TaskDetails.ProjectID);
            
            this.projectdata = response; 
            this.projectdata = this.projectdata.find(p=>p.ProjectID == this.TaskDetails.ProjectID);
            console.log('prkect data' + this.projectdata );  
            this.ProjectName= this.projectdata.Project; 
            
            this.ServiceService.getParentTaskData().subscribe
            (response=>
              {
          
              console.log("response" + JSON.stringify(response));
              
               this.ParentTaskdata = response;    
               console.log('ParetID' + this.TaskDetails.ParentID);
               this.ParentTaskdata = this.ParentTaskdata.find(p=>p.ParentID == this.TaskDetails.ParentID);
               this.parentTask=this.ParentTaskdata.ParentTask ;

               this.ServiceService.getUserData().subscribe
                  (response=>
                    {

                    console.log("response" + JSON.stringify(response));
                    
                    this.usersdata = response;    
                    console.log ('Username' + this.TaskDetails.TaskOwner);
                    this.usersdata = this.usersdata.find(p=>p.UserID == this.TaskDetails.TaskOwner);
                    this.UserName=this.usersdata.FirstName + ' ' + this.usersdata.LastName ;
                    console.log('this.usersdata ', this.usersdata);
                    console.log('username '+ this.UserName);
                    //this.usersdata.StartDate.spl;
                    // console.log('kkk'+ this.TaskDetails.StartDate);
                    }
                    );
              // console.log('kkk'+ this.TaskDetails.StartDate);
              }
              );
            
            //console.log('kkk'+ this.TaskDetails.StartDate);
            }
            );
        //     this.apiservice.getUserData().subscribe(response=>
        //       {
          
        //         console.log("response",response);
        //         this.usersdata = response;
        //         this.userDetails = this.usersdata.find(p=>p.User_ID == this.taskDetails.TASK_OWNER_ID);                
        //         this.taskDetails.UserName = this.userDetails.FirstName + " " + this.userDetails.LastName;
        //       } );
        //       if(this.TaskDetails.Parent_ID != null)
        //       {
        //         this.ServiceService.getParenTaskData().subscribe(response=>
        //           {
              
        //             console.log("response",response);
        //             this.parentTaskData = response;
        //             this.parenTaskDetails = this.parentTaskData.find(p=>p.Parent_ID == this.TaskDetails.Parent_ID);  
        //             this.TaskDetails.ParentTask = this.parenTaskDetails.Parent_Task;
        //           } );

        //       }
         } );
       
    }
    
  }
 
  Add(){
    // if((this.ProjectName!=="" && this.ProjectName!==undefined) && (this.TaskDetails.Task!=="" &&
    // this.TaskDetails.Task!==undefined && this.TaskDetails.Task!==null)
    //  && (this.parentTask!="" && this.parentTask!==undefined && this.parentTask!==null)
    //  &&(this.UserName!=="" && this.UserName!==undefined && this.UserName!==null )
    // && (this.TaskDetails.StartDate!=="" && this.TaskDetails.StartDate!==null && 
    // this.TaskDetails.StartDate!== undefined) &&
    // (this.TaskDetails.EndDate!=="" && this.TaskDetails.EndDate!==null && 
    // this.TaskDetails.EndDate!== undefined)
    // )  {
    //   this.isEmpty=true;
      if(this.IsParentTask == true)
    {
      if(this.TaskDetails.Task=="" || this.TaskDetails.Task==null || this.TaskDetails.Task==undefined)
      {
        this.isTaskEmpty=false;
      }
      else{ 
        this.isTaskEmpty=true;
      console.log('check'+ this.IsParentTask)
      this.ParentTaskDetail.ParentTask = this.TaskDetails.Task;
      this.ServiceService.postParentTaskData(this.ParentTaskDetail)
      .subscribe(response=>
        {
  
          console.log("response",response);
          //this.ptask = response;
          
        } );
        this.IsParentTask = false;
      }
       // this.TaskDetails.Task = '';
    }
    else
    {
       if((this.ProjectName=="" || this.ProjectName==undefined) && (this.TaskDetails.Task=="" ||
       this.TaskDetails.Task==undefined|| this.TaskDetails.Task==null)
        && (this.parentTask=="" || this.parentTask==undefined || this.parentTask==null)
        &&(this.UserName=="" || this.UserName==undefined || this.UserName==null )
       //&& (this.TaskDetails.StartDate!=="" && this.TaskDetails.StartDate!==null && 
     // this.TaskDetails.StartDate!== undefined) &&
       //(this.TaskDetails.EndDate!=="" && this.TaskDetails.EndDate!==null && 
      // this.TaskDetails.EndDate!== undefined)
       )
      {
        this.isTaskEmpty=false; 
       this.isProject=false; 
       this.isParentTask=false; 
       this.isUser=false; 
      }
      else if((this.ProjectName=="" || this.ProjectName==undefined))
      {
        this.isProject=false; 
      }
      else if(this.TaskDetails.Task=="" ||
      this.TaskDetails.Task==undefined || this.TaskDetails.Task==null)
      {
        this.isTaskEmpty=false; 
      }
      // else if(this.parentTask="" || this.parentTask==undefined || this.parentTask==null)
      // {
      //   this.isParentTask=false; 
      // }

      else if((this.UserName=="" || this.UserName==undefined || this.UserName==null ))
      {
        this.isUser=false; 
      }
    else{ 
      this.isTaskEmpty=true; 
       this.isProject=true; 
       this.isParentTask=true; 
       this.isUser=true; 
      console.log('test'+  this.TaskDetails.Task);
    this.ServiceService.postTaskData(this.TaskDetails)    
        .subscribe(response=>
        {
          console.log(response);
         
          alert('Task Added!') ;     
        }
      
      );
    }
    }
   //}
    // else{
    //   this.isEmpty=false;
      
    // }
    
  }
  update()
  {
   console.log('comin in udate');
    // this.ParentTaskDetail.ParentTask = this.TaskDetails.Task;
     
   //this.TaskDetails.ParentID=this.ParentTaskDetail.ParentID;
    this.ServiceService.putTaskData(this.TaskDetails)    
        .subscribe(response=>
        {
          console.log(response);
         
          alert('Task Updated!') ;     
        }
      
      );
   
  }
    searchProject(){
      console.log('entererd');
      this.projectdisplay='block';
      this.columnsofProject =  this.ServiceService.getColumnsProjects();
    console.log(this.columnsofProject);
    this.ServiceService.getProjectData().subscribe
    (response=>
      {

      console.log("response" + JSON.stringify(response));
      
       this.projectdata = response;    
      
      //console.log('kkk'+ this.TaskDetails.StartDate);
      }
      );
    }
    selectproject(project){
       this.TaskDetails.ProjectID=project.ProjectID;
this.ProjectName= project.Project;
    }
    closeprojectDialog(){
this.projectdisplay='none';

    }

    searchUser(){
  this.userdisplay='block';
  this.columnsofUsers =  this.ServiceService.getColumnsUsers();
  console.log('test123' +this.columnsofUsers);
  this.ServiceService.getUserData().subscribe
  (response=>
    {

    console.log("response" + JSON.stringify(response));
    
     this.usersdata = response;    
    
    // console.log('kkk'+ this.TaskDetails.StartDate);
    }
    );

}

closeUserDialog(){
  this.userdisplay='none';
  
      }
  
      select(user){
  
  this.UserName=user.FirstName +' '+ user.LastName;
this.TaskDetails.TaskOwner= user.UserID; 
      }


      searchParentTask(){
        this.ParentTaskdisplay='block';
        this.columnsofParentTask =  this.ServiceService.getColumnsofParentTask();
        console.log('test123' +this.columnsofParentTask);
        this.ServiceService.getParentTaskData().subscribe
        (response=>
          {
      
          console.log("response" + JSON.stringify(response));
          
           this.ParentTaskdata = response;    
          
          // console.log('kkk'+ this.TaskDetails.StartDate);
          }
          );

      }
      closeParentTaskDialog(){
        this.ParentTaskdisplay='none';
        
            }
        
            selectParenTask(user){
        this.parentTask =user.ParentTask;
        this.TaskDetails.ParentID = user.ParentID;
            // this.UserName=user.FirstName +' '+ user.LastName;
      //this.TaskDetails.TaskOwner= user.EmployeeID; 
            }
cancel(){
     this.isAdd= false;
      this.isUpdate= true;
      
  this.TaskDetails.EndDate=null;
  this.TaskDetails.StartDate=null;
  this.TaskDetails.Task=null;
   this.IsParentTask=true;
  this.UserName=null;
   this.ProjectName=null;
   this.TaskDetails.Priority=0;
   this.isTaskEmpty=true;
   this.isTaskEmpty=true; 
       this.isProject=true; 
       this.isParentTask=true; 
       this.isUser=true; 

}            
            
}
