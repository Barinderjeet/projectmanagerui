import { Component, OnInit } from '@angular/core';
import {CommonModule} from "@angular/common";

import { ActivatedRoute } from '@angular/router';
import { ServiceService  } from 'src/app/services/service.service';
import { Router } from '@angular/router';
import {Task} from 'src/app/task';
import * as _ from 'lodash';


@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {

  constructor(private ServiceService: ServiceService,   private router: Router) { }
  TaskDetails : Task;
  columns : any;
  columnsofProject: any ;
  projectdisplay: string;
  projectdata: any;
  ProjectName: any;
  ProjectID:any;
  sortedOrder: string;
  SortedBy: string;
  ngOnInit() {
    this.projectdisplay='none';
    this.columns =  this.ServiceService.getColumns();
    console.log(this.columns);
    this.ServiceService.getTaskData().subscribe
    (response=>
      {

      console.log("response" + JSON.stringify(response));
      
       this.TaskDetails = response;    
      
      // console.log('kkk'+ this.TaskDetails.StartDate);
      }
      );
       
  }

  search(){
    this.projectdisplay='block';
    this.columnsofProject =  this.ServiceService.getColumnsProjects();
  console.log(this.columnsofProject);
  this.ServiceService.getProjectData().subscribe
  (response=>
    {

    console.log("response" + JSON.stringify(response));
    
     this.projectdata = response;    
    
    //console.log('kkk'+ this.TaskDetails.StartDate);
    }
    );
  }
  selectproject(project){
    this.TaskDetails.ProjectID=project.ProjectID;  
    this.ProjectName= project.ProjectID;
     
 }
 closeprojectDialog(){
this.projectdisplay='none';

 }

 sortUsers(sortBy: string) {
  console.log(sortBy);
  var sortOrder = this.SortedBy != sortBy || this.sortedOrder == "desc" ? "asc" : "desc";

  this.TaskDetails = _.orderBy(this.TaskDetails, [sortBy], [sortOrder]);
  console.log(this.TaskDetails);
  console.log(sortOrder);

  this.SortedBy = sortBy;

  this.sortedOrder = sortOrder;

}


editPage(col)
{
  console.log(col);
this.router.navigate(['update/' + col.TaskID]);
}


  deleteTask(col)
  {
    console.log("taskid",col.TaskID);
    this.ServiceService.deleteTaskData(col.TaskID)
    .subscribe(response=> {
      this.TaskDetails= response;
    });
  }

 endTask(col)
 {
   this.ServiceService.endTaskData(col)
   .subscribe(response=>{
     this.TaskDetails= response;
   });
 } 
}
