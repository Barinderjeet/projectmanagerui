import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, AppRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectComponent } from './ui/project/project.component';
import { AddTaskComponent } from './ui/add-task/add-task.component';
import { ViewTaskComponent } from './ui/view-task/view-task.component';
import { UserComponent } from './ui/user/user.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ViewTaskFilterPipe } from 'src/app/ui/view-task/ViewTaskFilter';
import { searchUserFilterPipe } from 'src/app/ui/user/UserSearchPipe';

@NgModule({
  declarations: [
    AppComponent,
    ViewTaskFilterPipe,
    ProjectComponent,
    AddTaskComponent,
    ViewTaskComponent,
    UserComponent,
    searchUserFilterPipe
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpClientModule
  ], 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
