
    import { Pipe, PipeTransform } from '@angular/core';
    import { NgAnalyzedFile } from "@angular/compiler";
    import { Task } from 'src/app/Task';

    @Pipe({

        name: "viewTaskFilter"
    })

export class ViewTaskFilterPipe implements PipeTransform{
        transform(View: Task[], SearchDetails: string ) {
            if(!View || !SearchDetails)
            {
                return View;
            }
            else{
                
                return View.filter(task=>
                    (SearchDetails && task.ProjectID.toString().
                    indexOf(SearchDetails.toString())  != -1)
    
                );
            }

            
        }


}