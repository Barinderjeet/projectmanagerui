import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/Project';
import { NgModule } from '@angular/core';
import { ServiceService  } from 'src/app/services/service.service';
import { User } from 'src/app/User';
import * as _ from 'lodash';    
import { Task } from 'src/app/Task';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  users: User;
  completedtask: number;
  nooftask: number;
  TaskDetails: Task[];
  sortedOrder: string;
  SortedBy: string;
  userdisplay: string;
  columnsofUsers: string[];
  ProjectList: any;
  usersdata:  Array<User>;
  isAdd: boolean= false;
  isUpdate : boolean= true;

  isProjectEmpty: boolean=true;
  isManagerEmpty: boolean=true;
  isStartDateEmpty: boolean=true;
  isEndDateEmpty: boolean=true;

  constructor(private ServiceService: ServiceService) { }

 columns: any;
 ManagerNmae: string;
 ProjectDetails : Project = { ProjectID:0, Project:null, Priority:null, ManagerID:null, StartDate:null, EndDate:null, Flag:0};
  ngOnInit() {
    this.userdisplay='none';
    this.columns =  this.ServiceService.getColumnsProjects();
    console.log(this.columns);
    this.getProjectData();
    this.ServiceService.getTaskData().subscribe
    (response=>
      {

      console.log("response" + JSON.stringify(response));
      
       this.TaskDetails = response;    
      
      // console.log('kkk'+ this.TaskDetails.StartDate);
      }
      );
  }

  getProjectData(){
    this.ServiceService.getProjectData().subscribe
    (response=>
      {

      console.log("response" + JSON.stringify(response));
      
       this.ProjectList = response;    
      //this.ProjectList.reverse();
      //console.log('kkk'+ this.TaskDetails.StartDate);
      }
      );

  }
  add(){
    
    if((this.ManagerNmae=="" || this.ManagerNmae==null || this.ManagerNmae==undefined) &&
    (this.ProjectDetails.Project=="" || this.ProjectDetails.Project==null || this.ProjectDetails.Project==undefined)
  && (this.ProjectDetails.StartDate=="" || this.ProjectDetails.StartDate==null || this.ProjectDetails.StartDate==undefined)
&& (this.ProjectDetails.EndDate=="" || this.ProjectDetails.EndDate==null || this.ProjectDetails.EndDate==undefined))
    {
      this.isProjectEmpty=false;
      this.isManagerEmpty=false;
      this.isStartDateEmpty=false;
      this.isEndDateEmpty=false;

    }
 if(this.ProjectDetails.Project=="" || this.ProjectDetails.Project==null || this.ProjectDetails.Project==undefined)
 {
   this.isProjectEmpty=false;
 }
 else if(this.ProjectDetails.StartDate=="" || this.ProjectDetails.StartDate==null || this.ProjectDetails.StartDate==undefined)
 {
   this.isStartDateEmpty=false;
 }
 else if(this.ProjectDetails.EndDate=="" || this.ProjectDetails.EndDate==null || this.ProjectDetails.EndDate==undefined)
 {
   this.isEndDateEmpty=false;
 }
 else if(this.ManagerNmae=="" || this.ManagerNmae==null || this.ManagerNmae==undefined)
 {
   this.isManagerEmpty=false;
 }
 else{
  this.isProjectEmpty=true;
  this.isManagerEmpty=true;
  this.isStartDateEmpty=true;
  this.isEndDateEmpty=true;
    this.ServiceService.postProjectData(this.ProjectDetails)    
        .subscribe(response=>
        {
          console.log(response);
         
          alert('Project Added!') ;  
          this.getProjectData();   
        }
      
      );
    }
    }

    search(){
      this.userdisplay='block';
      this.columnsofUsers =  this.ServiceService.getColumnsUsers();
      console.log('test123' +this.columnsofUsers);
      this.ServiceService.getUserData().subscribe
      (response=>
        {
  
        console.log("response" + JSON.stringify(response));
        
         this.usersdata = response;    
        
        // console.log('kkk'+ this.TaskDetails.StartDate);
        }
        );
    }

    closeUserDialog(){
this.userdisplay='none';

    }

    select(user){

this.ManagerNmae=user.FirstName +' '+ user.LastName;
this.ProjectDetails.ManagerID= user.UserID; 
    }

    reset(){
      this.isAdd= false;
      this.isUpdate= true;
      this.ProjectDetails.Project="";
     this.ProjectDetails.StartDate="";
      this.ProjectDetails.EndDate="";
      this.ProjectDetails.Priority=null;
      this.ManagerNmae=null;
    
      this.isProjectEmpty=true;
  this.isManagerEmpty=true;
  this.isStartDateEmpty=true;
  this.isEndDateEmpty=true;

    }

    sortUsers(sortBy: string) {
      console.log(sortBy);
      var sortOrder = this.SortedBy != sortBy || this.sortedOrder == "desc" ? "asc" : "desc";
  
      this.ProjectList = _.orderBy(this.ProjectList, [sortBy], [sortOrder]);
      console.log(this.ProjectList);
      console.log(sortOrder);
  
      this.SortedBy = sortBy;
  
      this.sortedOrder = sortOrder;
  
    } 

    Edit(ProjectID) {
      this.isAdd= true;
    this.isUpdate= false;
      //this.edited = true;
      //this.updatedisplay = 'block';
      //this.adddisplay = 'none';
      //this.ParentTaskdata = this.ParentTaskdata.find(p=>p.ParentID == this.TaskDetails.ParentID);
             //  this.parentTask=this.ParentTaskdata.ParentTask ;
      console.log('test' + this.ProjectDetails);
      this.ProjectDetails = {...this.ProjectList.find(p => p.ProjectID == ProjectID)};
      
      this.ProjectDetails.StartDate = this.ProjectDetails.StartDate.split('T')[0];
      this.ProjectDetails.EndDate = this.ProjectDetails.EndDate.split('T')[0];
      console.log('project dtls' +  JSON.stringify(this.ProjectDetails));
     
      this.ServiceService.getUserData().subscribe
      (response=>
        {
  
        console.log("response" + JSON.stringify(response));
        
         this.usersdata = response;    
         this.users =this.usersdata.find(p=> p.UserID == this.ProjectDetails.ManagerID);
         this.ManagerNmae = this.users.FirstName + " " + this.users.LastName;
        // console.log('kkk'+ this.TaskDetails.StartDate);
        }
        );
        console.log('user dtls' +  JSON.stringify( this.usersdata));
      
      // console.log('ManagerName', this.ProjectDetails.ManagerID );
     //this.ProjectDetails.ManagerID= user.UserID; 
  
    }
  
    Delete(ProjectID) 
      {
        console.log("taskid", ProjectID);
         this.ServiceService.deleteProjectData(ProjectID)
         .subscribe(response=> {
          this.ProjectDetails= response;
           this.getProjectData();
        
        }); 
      }
      getTotalTask(ProjectID)
      {

        this.nooftask = this.TaskDetails.filter(p=>p.ProjectID == ProjectID).length;
        return this.nooftask;
      }
      getCompletedTask(ProjectID,project)
      {
        this.completedtask = this.TaskDetails.filter(p=>p.ProjectID == ProjectID && p.Status == "1").length;
        project.CompletedTask =this.completedtask; 
        return this.completedtask;

      }

      update()
      {
        this.ServiceService.putProjectData(this.ProjectDetails)    
        .subscribe(response=>
        {
          console.log(response);
         
          alert('Project Updated!') ;  
          this.getProjectData();   
        }
      
      );

      }
}
